using System;
using UFV.Business.AtaMath;
using UnityEngine;

namespace UFV.Extensions 
{
    public static class UnityExtensions {
        public static LatLong ToLatLong(this Quaternion quaternion)
        {
            return new LatLong
            {
                LatRads = Math.Acos(quaternion.normalized.y) - Math.PI / 2,
                LongRads = Math.Atan2(quaternion.normalized.x, quaternion.normalized.z) - Math.PI
            };
        }
    }
}