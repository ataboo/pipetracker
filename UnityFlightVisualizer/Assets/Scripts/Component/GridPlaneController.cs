﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UFV.Business.AtaMath;
using UFV.Business.GridRender;
using UFV.Business.SRTMParser;
using UnityEditor;
using UnityEngine;

public class GridPlaneController : MonoBehaviour
{
    private MeshRenderer _renderer;
    private MeshFilter _meshFilter;

    public int Latitude = 53;
    public int Longitude = -114;

    private void Start()
    {
        _renderer = GetComponent<MeshRenderer>();
        _meshFilter = GetComponent<MeshFilter>();
    }

    public async Task SetSRTMGrid(SRTMGrid grid)
    {
        //var renderer = new GridRenderer();

        //_meshFilter.mesh = await renderer.PlaneFromGrid(grid);
        //_renderer.material.SetTexture("_MainTex", await renderer.TextureFromGrid(grid));
        
    }
}
