﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UFV.Business.AtaMath;
using UFV.Business.SRTMParser;
using UnityEngine;

public class BusinessController : MonoBehaviour
{
    Quaternion angle1;
    bool _run = false;

    void Start()
    {
        //FindObjectOfType<Camera>().transform.position = new LatLong(53.5, -113.5).PosAtAltitude(1000);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(Vector3.zero, 5);
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(angle1 * Vector3.forward * 5, 0.2f);
    }

    async void Update()
    {
        //if (!_run)
        //{
        //    _run = true;

        //    var config = new SRTMConfig
        //    {
        //        GridSize = new Vector2Int(3601, 3601),
        //        HGTFolder = Application.streamingAssetsPath,
        //        IMGFolder = Application.streamingAssetsPath,
        //    };

        //    var parser = new SRTMParser(config);

        //    Debug.Log($"Start grid {Time.time}");

        //    var grid = await parser.ParseGrid(53, -114);

        //    Debug.Log($"Done grid {Time.time}");

        //    var planeController = FindObjectOfType<GridPlaneController>();

        //    await planeController.SetSRTMGrid(grid);
        //}

        //Debug.DrawLine(Vector3.zero, Vector3.forward, Color.blue);
        //Debug.DrawLine(Vector3.zero, Vector3.up, Color.green);
        //Debug.DrawLine(Vector3.zero, Vector3.right, Color.red);

        //Debug.DrawLine(Vector3.zero, angle1 * Vector3.forward * 5, Color.yellow);
    }
}
