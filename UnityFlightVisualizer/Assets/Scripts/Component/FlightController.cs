﻿using UnityEngine;

public class FlightController : MonoBehaviour
{
    public float rollSpeed = 20f;
    public float forwardSpeed = 60f;
    public float pitchSpeed = 20f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateControls();

        MoveForward();
    }

    void UpdateControls()
    {
        var rollControl = Input.GetAxis("Horizontal");
        var pitchControl = Input.GetAxis("Vertical");
        transform.Rotate(new Vector3(pitchControl * pitchSpeed, 0, -rollControl * rollSpeed) * Time.deltaTime);
    }

    void MoveForward()
    {
        transform.position += transform.forward * forwardSpeed * Time.deltaTime;
    }
}
