﻿using UnityEngine;
using UnityEditor;
using System.Threading.Tasks;
using UFV.Business.SRTMParser;
using UFV.Business.GridRender;
using System.Collections.Generic;
using System.Threading;
using System;
using System.Collections;
using System.IO;

// Simple Editor Script that fills a bar in the given seconds.
public class EditorGroundMeshGenerator : EditorWindow
{
    private readonly Vector2Int GRID_SIZE = new Vector2Int(3601, 3601);
    public int latitude = 53;
    public int longitude = -114;

    private bool _startMeshGeneration = false;
    private TerrainMesh _queuedTerrainMesh;
    private SRTMGrid _grid;
    private int _progress = -1;
    private int _progressTotal = 10;
    private string _progressTitle = "Generating SRTM3 Terrain Mesh";
    private string _progressDescription = "Starting";

    [MenuItem("UFV/Terrain Mesh Generator")]
    static void Init()
    {
        EditorWindow window = GetWindow(typeof(EditorGroundMeshGenerator));
        window.Show();
    }

    private async Task Update()
    {
        if (_startMeshGeneration)
        {
            _startMeshGeneration = false;
            var terrainMesh = Task.Run(() => GenerateMesh());
            //var terrainMesh = Task.Run(GenerateMesh);

        }
    }

    private void OnGUI()
    {
        EditorGUI.BeginDisabledGroup(_progress >= 0);
        latitude = EditorGUILayout.IntField("Latitude:", latitude);
        longitude = EditorGUILayout.IntField("Longitude:", longitude);
        
        if (GUILayout.Button("Save Mesh"))
        {
            _startMeshGeneration = true;
        }
        EditorGUI.EndDisabledGroup();

        if (_progress >= 0) {
            EditorUtility.DisplayProgressBar(_progressTitle, _progressDescription, (float)_progress / _progressTotal);
        }
        else
        {
            EditorUtility.ClearProgressBar();
        }

        if (_queuedTerrainMesh != null)
        {
            var mesh = new Mesh();
            mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            mesh.name = _queuedTerrainMesh.Name;
            mesh.vertices = _queuedTerrainMesh.Vertices;
            mesh.triangles = _queuedTerrainMesh.Tris;
            mesh.uv = _queuedTerrainMesh.UVs;
            MeshUtility.SetMeshCompression(mesh, ModelImporterMeshCompression.Medium);
            _queuedTerrainMesh = null;

            EditorUtility.DisplayProgressBar(_progressTitle, "Optimizing mesh", 0.2f);
            mesh.OptimizeIndexBuffers();

            EditorUtility.DisplayProgressBar(_progressTitle, "Optimizing mesh", 0.4f);
            mesh.OptimizeReorderVertexBuffer();

            EditorUtility.DisplayProgressBar(_progressTitle, "Saving mesh", 0.6f);
            AssetDatabase.CreateAsset(mesh, $"Assets/Meshes/Terrain/{mesh.name}.mesh");
            AssetDatabase.SaveAssets();

            EditorUtility.DisplayProgressBar(_progressTitle, "Generating texture", 0.8f);

            var gridRenderer = new GridRenderer();
            var texture = gridRenderer.TextureFromGrid(_grid).GetAwaiter().GetResult();
            var png = texture.EncodeToPNG();

            EditorUtility.DisplayProgressBar(_progressTitle, "Saving texture", 0.9f);

            var pngPath = Path.Combine(Application.dataPath, $"Textures/Terrain/{mesh.name}.png");
            using (var pngFile = File.Create(pngPath))
            {
                pngFile.Write(png, 0, png.Length);
            }

            _progress = -1;
        }
    }

    void OnInspectorUpdate()
    {
        Repaint();
    }

    async Task GenerateMesh()
    {
        _progressDescription = "Parsing SRTM files.";
        _progress = 0;

        var parser = new SRTMParser(new SRTMConfig
        {
            GridSize = GRID_SIZE,
            HGTFolder = Application.streamingAssetsPath,
            IMGFolder = Application.streamingAssetsPath, 
        });

        ProgressEventHandler onProgressUpdate = (_, progress, total) =>
        {
            _progressTotal = total;
            _progress = progress;
        };

        parser.ProgressEvent += onProgressUpdate;
        _grid = await parser.ParseGrid(53, -114);
        parser.ProgressEvent -= onProgressUpdate;

        _progressDescription = "Generating Plane from Grid.";
        _progress = 0;

        var gridRenderer = new GridRenderer();
        gridRenderer.ProgressEvent += onProgressUpdate;

        var tasks = new List<Task>();

        var terrainMesh = await gridRenderer.PlaneFromGrid(_grid);

        _queuedTerrainMesh = terrainMesh;
    }
}    

