﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UFV.Business.AtaMath;
using UFV.Business.SRTMParser;
using UnityEngine;

namespace UFV.Business.GridRender
{
    public class GridRenderer : IGridRenderer
    {
        public event ProgressEventHandler ProgressEvent;

        public async Task<TerrainMesh> PlaneFromGrid(SRTMGrid grid)
        {
            var baseLatLong = new LatLong(grid.LatLongMin.x + 0.5, grid.LatLongMin.y + 0.5);
            var baseRadius = grid.Points[0][0].HeightASL + GeoPosition.SeaLevelAtLatLong(baseLatLong);
            var basePos = baseLatLong.ToQuaternion() * Vector3.forward * (float)baseRadius;

            var mesh = new TerrainMesh
            {
                GridSize = grid.Size,
                Name = $"srtm_{baseLatLong.WholeNLatELong}",
                Scale = 0.5f,
            };

            mesh.Vertices = new Vector3[mesh.PointTotal];
            mesh.Tris = new int[6 * (mesh.Size.x - 1) * (mesh.Size.y - 1)];
            mesh.UVs = new Vector2[mesh.PointTotal];

            ProgressEvent.Invoke(this, 0, mesh.PointTotal);

            Debug.Log("Plane from grid running!");

            var tasks = new List<Task>();
            for (int iy = 0; iy < mesh.Size.y; iy++)
            {
                for (int ix = 0; ix < mesh.Size.x; ix++)
                {
                    var thisX = ix;
                    var thisY = iy;
                    tasks.Add(MakePosition(mesh, basePos, grid, thisX, thisY));
                }
            }

            var taskArr = tasks.ToArray();

            //var whenAll = Task.WhenAll(taskArr);

            var count = 0;
            foreach(var task in taskArr)
            {
                await task;
                ProgressEvent.Invoke(this, ++count, taskArr.Length);
            }

            return mesh;
        }

        private async Task MakePosition(TerrainMesh mesh, Vector3 basePos, SRTMGrid grid, int ix, int iy)
        {
            var latitude = grid.LatLongMin.x + (mesh.Size.x - (float)iy - 1) / (mesh.Size.y - 1);
            var longitude = grid.LatLongMin.y + (float)ix / (mesh.Size.x - 1);

            var latLong = new LatLong(latitude, longitude);

            var scaledIx = (int)(ix / mesh.Scale);
            var scaledIy = (int)(iy / mesh.Scale);

            var realPos = latLong.PosAtAltitude(grid.Points[scaledIy][scaledIx].HeightASL);
            var transposed = realPos - basePos;
            var rotated = Quaternion.Euler(-90, 0, 0) * Quaternion.Inverse(latLong.ToQuaternion()) * transposed;
            mesh.Vertices[iy * mesh.Size.x + ix] = rotated;
            mesh.UVs[iy * mesh.Size.x + ix] = new Vector2((float)ix / (mesh.Size.x - 1), (float)iy / (mesh.Size.y - 1));

            if (iy < mesh.Size.y - 1 && ix < mesh.Size.x - 1)
            {
                // a -- d
                // | \\ |
                // b -- c
                var a = iy * mesh.Size.x + ix;
                var b = (iy + 1) * mesh.Size.x + ix;
                var c = (iy + 1) * mesh.Size.x + ix + 1;
                var d = iy * mesh.Size.x + ix + 1;

                var idx = iy * (mesh.Size.x - 1) + ix;
                mesh.Tris[6 * idx] = a;
                mesh.Tris[6 * idx + 1] = c;
                mesh.Tris[6 * idx + 2] = b;
                mesh.Tris[6 * idx + 3] = a;
                mesh.Tris[6 * idx + 4] = d;
                mesh.Tris[6 * idx + 5] = c;
            }
        }

        public async Task<Texture2D> TextureFromGrid(SRTMGrid grid)
        {
            var texture = new Texture2D(grid.Size.x, grid.Size.y);
            //texture.filterMode = FilterMode.Point;
            //texture.anisoLevel = 0;

            var min = float.MaxValue;
            var max = float.MinValue;
            for (int ix = 0; ix < grid.Size.x; ix++)
            {
                for (int iy = 0; iy < grid.Size.y; iy++)
                {
                    var pixel = grid.Points[ix][iy].ImgPixel;
                    max = Math.Max(pixel, max);
                    if (pixel > 0)
                    {
                        min = Math.Min(pixel, min);
                    }
                }
            }

            var spread = max - min;

            for (int iy = 0; iy < grid.Size.y; iy++)
            {
                for (int ix = 0; ix < grid.Size.x; ix++)
                {
                    var scaled = (grid.Points[iy][ix].ImgPixel - min) / (float)spread;
                    texture.SetPixel(ix, iy, GradientColor(scaled));
                }
            }

            texture.Apply();

            return texture;
        }

        public Color GradientColor(float t)
        {
            return Color.Lerp(Color.black, Color.green, t);
        }
    }
}
