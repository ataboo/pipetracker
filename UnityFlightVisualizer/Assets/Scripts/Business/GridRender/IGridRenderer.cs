﻿using System.Threading.Tasks;
using UFV.Business.SRTMParser;
using UnityEngine;

namespace UFV.Business.GridRender
{
    public interface IGridRenderer
    {
        event ProgressEventHandler ProgressEvent;

        Task<TerrainMesh> PlaneFromGrid(SRTMGrid grid);
    }
}
