﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace UFV.Business.GridRender
{
    public class TerrainMesh
    {
        public int PointTotal => Size.x * Size.y;

        public Vector2Int GridSize { get; set; }

        public string Name { get; set; }

        public Vector2Int Size => new Vector2Int((int)(GridSize.x * Scale), (int)(GridSize.y * Scale));

        public Vector3[] Vertices { get; set; }
        
        public Vector2[] UVs { get; set; }

        public int[] Tris { get; set; }

        public float Scale { get; set; }
    }
}
