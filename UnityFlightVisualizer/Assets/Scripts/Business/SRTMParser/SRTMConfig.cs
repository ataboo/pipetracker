﻿using UnityEngine;

namespace UFV.Business.SRTMParser
{
    public class SRTMConfig
    {
        public string HGTFolder { get; set; }

        public string IMGFolder { get; set; }

        public Vector2Int GridSize { get; set; }
    }
}
