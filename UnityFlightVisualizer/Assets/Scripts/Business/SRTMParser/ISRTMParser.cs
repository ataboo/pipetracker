﻿using System.Threading.Tasks;
using UFV.Business.AtaMath;
using System;

namespace UFV.Business.SRTMParser
{
    public delegate void ProgressEventHandler(object sender, int progress, int total);

    public interface ISRTMParser
    {
        event ProgressEventHandler ProgressEvent;

        Task<SRTMGrid> ParseGrid(int latitude, int longitude);

        Task<SRTMGrid> ParseGrid(LatLong latLong);
    }
}
