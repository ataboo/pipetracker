﻿using UnityEngine;

namespace UFV.Business.SRTMParser
{
    public class SRTMGrid
    {
        public Vector2Int LatLongMin { get; set; }

        public Vector2Int Size { get; set; }

        public SRTMPoint[][] Points { get; set; }
    }
}
