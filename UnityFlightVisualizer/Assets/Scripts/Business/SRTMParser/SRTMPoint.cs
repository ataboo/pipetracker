﻿using System;

namespace UFV.Business.SRTMParser
{
    public class SRTMPoint
    {
        public Int16 HeightASL { get; set; }
        
        public byte ImgPixel { get; set; }
    }
}
