using System.IO;
using System.Threading.Tasks;
using UFV.Business.AtaMath;
using UnityEngine;

namespace UFV.Business.SRTMParser {
    public class SRTMParser : ISRTMParser {
        private readonly SRTMConfig _config;

        public event ProgressEventHandler ProgressEvent;

        public SRTMParser(SRTMConfig config)
        {
            _config = config;
        }

        public async Task<SRTMGrid> ParseGrid(LatLong latLong)
        {
            return await ParseGrid((int)latLong.LatDeg, (int)latLong.LongDeg);
        }

        public async Task<SRTMGrid> ParseGrid(int latitude, int longitude)
        {
            return new SRTMGrid()
            {
                Size = _config.GridSize,
                LatLongMin = new Vector2Int(latitude, longitude),
                Points = await ParsePoints(latitude, longitude),
            };
        }

        private async Task<SRTMPoint[][]> ParsePoints(int latitude, int longitude)
        {
            var progressTotal = _config.GridSize.x * _config.GridSize.y + 3000;
            ProgressEvent.Invoke(this, 0, progressTotal);

            var points = new SRTMPoint[_config.GridSize.y][];
            var heightsPath = SRTMHeightPath(latitude, longitude);
            var imgPath = SRTMImgPath(latitude, longitude);

            // int16s
            var heightByteCount = _config.GridSize.y * _config.GridSize.x * 2; 
            var heightBytes = new byte[heightByteCount];

            using (var heightFile = File.OpenRead(heightsPath))
            {
                if (heightFile.Length != heightByteCount)
                {
                    throw new FileLoadException("SRTM height file not expected size.");
                }

                var n = await heightFile.ReadAsync(heightBytes, 0, heightByteCount);
                if (n != heightByteCount)
                {
                    throw new FileLoadException("Not enough bytes read from SRTM height file.");
                }
            }

            ProgressEvent.Invoke(this, 1500, progressTotal);

            // uint8s
            var imgByteCount = _config.GridSize.y * _config.GridSize.x;
            var imgBytes = new byte[imgByteCount];
            using (var imgFile = File.OpenRead(imgPath))
            {
                if (imgFile.Length != imgByteCount)
                {
                    throw new FileLoadException("SRTM img file not expected size.");
                }

                var n  = await imgFile.ReadAsync(imgBytes, 0, imgByteCount);
                if (n != imgByteCount)
                {
                    throw new FileLoadException("Unexpected img SRTM read count.");
                }
            }

            ProgressEvent.Invoke(this, 3000, progressTotal);

            for (int iy = 0; iy < _config.GridSize.y; iy++)
            {
                points[iy] = new SRTMPoint[_config.GridSize.x];
                for (int ix = 0; ix < _config.GridSize.x; ix++)
                {
                    var idx = (iy * _config.GridSize.x + ix) * 2;
                    short int16Val = (short)(heightBytes[idx] << 8 | heightBytes[idx + 1]);

                    points[iy][ix] = new SRTMPoint
                    {
                        HeightASL = int16Val,
                        ImgPixel = imgBytes[iy*_config.GridSize.x + ix],
                    };

                    ProgressEvent.Invoke(this, iy * _config.GridSize.x + ix, progressTotal);
                }
            }

            ProgressEvent.Invoke(this, progressTotal, progressTotal);

            return points;
        }

        private string SRTMHeightPath(int latitude, int longitude)
        {
            return Path.Combine(_config.HGTFolder, $"{SRTMFileBase(latitude, longitude)}.hgt");
        }

        private string SRTMImgPath(int latitude, int longitude)
        {
            return Path.Combine(_config.IMGFolder, $"{SRTMFileBase(latitude, longitude)}.img");
        }

        private string SRTMFileBase(int latitude, int longitude)
        {
            return new LatLong(latitude, longitude).WholeNLatELong;
        }
    }
}
