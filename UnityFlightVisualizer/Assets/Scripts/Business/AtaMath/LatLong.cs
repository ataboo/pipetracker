﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace UFV.Business.AtaMath
{
    public class LatLong
    {
        private double _latRads;
        public double LatRads {
            get => _latRads;
            set
            {
                if (value < -Math.PI / 2 || value > Math.PI / 2)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _latRads = value;
            }
        }

        private double _longRads;
        public double LongRads
        {
            get => _longRads;
            set
            {
                if (value < -Math.PI || value > Math.PI)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _longRads = value;
            }
        }

        public double LatDeg
        {
            get => _latRads * 180d / Math.PI;
            set
            {
                LatRads = value * Math.PI / 180d;
            }
        }

        public double LongDeg
        {
            get => _longRads * 180d / Math.PI;
            set
            {
                LongRads = value * Math.PI / 180d;
            }
        }

        public string LatCardinal => (LatDeg < 0 ? "S" : "N");

        public string LongCardinal => (LongDeg < 0 ? "W" : "E");

        public LatLong() { }

        public LatLong(double latDegs, double longDegs)
        {
            LatDeg = latDegs;
            LongDeg = longDegs;
        }

        /// <summary>
        /// ex. N12W123
        /// </summary>
        public string WholeNLatELong => $"{LatCardinal}{Math.Abs((int)LatDeg).ToString("D2")}{LongCardinal}{Mathf.Abs((int)LongDeg).ToString("D3")}";

        public Quaternion ToQuaternion()
        {
            return Quaternion.Euler((float)-LatRads * Mathf.Rad2Deg, (float)-LongRads * Mathf.Rad2Deg, 0);
        }

        public Vector3 PosAtAltitude(float altitudeASL)
        {
            var radius = GeoPosition.SeaLevelAtLatLong(this);

            return ToQuaternion() * Vector3.forward * (float)(radius + altitudeASL);
        }
    }
}
