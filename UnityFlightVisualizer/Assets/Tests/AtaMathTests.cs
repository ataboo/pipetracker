﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UFV.Business.AtaMath;

namespace Tests
{
    public class AtaMathTests
    {
        [TestCase(0, 6378137)]
        [TestCase(90, 6356752)]
        [TestCase(-90, 6356752)]
        [TestCase(45, 6367489)]
        public void AtaMathTestsSimplePasses(double latDegs, int seaLevel)
        {
            Assert.AreEqual(seaLevel, (int)GeoPosition.SeaLevelAtLatLong(new LatLong(latDegs, 0)));
        }
    }
}
