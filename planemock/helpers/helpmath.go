package helpers

// MaxInt get the greater of 2 integers.
func MaxInt(a int, b int) int {
	if a > b {
		return a
	}

	return b
}

// MinInt get the lesser of 2 integers.
func MinInt(a int, b int) int {
	if a < b {
		return a
	}

	return b
}
