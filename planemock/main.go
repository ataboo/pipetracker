package main

import (
	"fmt"
	"os"

	"bitbucket.org/ataboo/pipetracker/planemock/mapdata"
	"bitbucket.org/ataboo/pipetracker/planemock/render"
	"golang.org/x/image/bmp"
)

func main() {
	// heightsTable, err := mapdata.ReadSrtmV3Heights("./../SRTMData/N53W114.hgt")
	// if err != nil {
	// 	log.Panic(err)
	// }

	// if err := DrawValsToBmp("/home/ataboo/Pictures/heights.bmp", heightsTable); err != nil {
	// 	log.Panic(err)
	// }

	// visualTable, err := mapdata.ReadSrtmV3Img("./../SRTMData/N53W114.img")
	// if err != nil {
	// 	log.Panic(err)
	// }

	// if err := DrawValsToBmp("/home/ataboo/Pictures/visual.bmp", visualTable); err != nil {
	// 	log.Panic(err)
	// }

	render.NewPlaneScene()

	fmt.Println("done!")
}

func DrawValsToBmp(path string, valTable [][]int) error {
	img := mapdata.ValTableToImg(valTable)
	out, err := os.Create(path)
	defer out.Close()
	if err != nil {
		return err
	}

	if err = bmp.Encode(out, img); err != nil {
		return err
	}

	return nil
}
