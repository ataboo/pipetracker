package planedata

import (
	"github.com/golang/geo/s2"
	"github.com/westphae/quaternion"
)

type AircraftData struct {
	Heading  quaternion.Quaternion `json:"heading"`
	Position s2.LatLng             `json:"position"`
}
