package client

import (
	"encoding/json"
	"fmt"
	"net"
	"sync"
	"time"

	"bitbucket.org/ataboo/pipetracker/planemock/planedata"
)

type TcpClient struct {
	conn net.Conn
	lock sync.Mutex
}

func (c *TcpClient) Start(hostAddr string) (connected chan error, err error) {
	if c.conn != nil {
		return connected, fmt.Errorf("already started")
	}

	connected = make(chan error)

	go func() {
		c.lock.Lock()
		defer c.lock.Unlock()

		fmt.Printf("dialing %s...", hostAddr)
		connectErr := c.tryToConnect(hostAddr, time.Minute)

		if connectErr == nil {
			fmt.Println("connected")
		} else {
			fmt.Println("failed to connect")
			fmt.Println(err)
		}
		select {
		case connected <- connectErr:
			//
		case <-time.After(time.Second):
			//
		}
	}()

	return connected, nil
}

func (c *TcpClient) Close() error {
	c.lock.Lock()
	c.lock.Unlock()

	if c.conn != nil {
		err := c.conn.Close()
		c.conn = nil

		return err
	}

	return nil
}

func (c *TcpClient) SendData(data *planedata.PlaneData) error {
	c.lock.Lock()
	defer c.lock.Unlock()

	if c.conn == nil {
		return fmt.Errorf("not connected")
	}

	rawJson, err := json.Marshal(data)
	if err != nil {
		return err
	}

	_, err = c.conn.Write(rawJson)

	return err
}

func (c *TcpClient) tryToConnect(address string, timeout time.Duration) error {
	var timeoutChan = time.After(timeout)

	for {
		conn, err := net.Dial("tcp", address)
		if err == nil {
			c.conn = conn
			return nil
		}

		select {
		case <-timeoutChan:
			return fmt.Errorf("timed out connection to %s.", address)
		case <-time.After(time.Second):
			break
		}
	}
}
