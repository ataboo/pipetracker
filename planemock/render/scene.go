package render

import (
	"math"
	"time"

	"bitbucket.org/ataboo/pipetracker/planemock/mapdata"
	"github.com/g3n/engine/app"
	"github.com/g3n/engine/camera"
	"github.com/g3n/engine/core"
	"github.com/g3n/engine/gls"
	"github.com/g3n/engine/light"
	"github.com/g3n/engine/math32"
	"github.com/g3n/engine/renderer"
	"github.com/g3n/engine/util/helper"
	"github.com/g3n/engine/window"
	"github.com/golang/geo/s2"
)

type PlaneControl struct {
}

type PlaneState struct {
	app   *app.Application
	scene *core.Node
	cam   *camera.Camera
}

type PlaneScene struct {
	Control *PlaneControl
	state   *PlaneState
}

func NewPlaneScene() *PlaneScene {
	scene := &PlaneScene{
		Control: &PlaneControl{},
		state:   &PlaneState{},
	}

	scene.Init()

	return scene
}

func (s *PlaneScene) Init() error {
	data, err := mapdata.ReadSrtmV3("./../SRTMData/N53W114.hgt", "./../SRTMData/N53W114.img")
	if err != nil {
		return err
	}

	s.state.app = app.App()
	s.state.scene = core.NewNode()
	s.state.cam = camera.New(1)
	camPos := mapdata.PositionFromLatLng(s2.LatLngFromDegrees(53.5, -113.5), 1500)
	s.state.cam.SetPosition(float32(camPos.X), float32(camPos.Y), float32(camPos.Z))
	// s.state.cam.SetRotationY(2 * -math.Pi / 3)

	// directionalLight := light.NewDirectional(math32.NewColor("Green"), 0.8)
	// directionalLight.SetPosition(float32(camPos.X), float32(camPos.Y), float32(camPos.Z))

	// s.state.scene.Add(directionalLight)

	ambientLight := light.NewAmbient(&math32.Color{1.0, 1.0, 1.0}, 0.8)
	ambientLight.SetPosition(s.state.cam.Position().X, s.state.cam.Position().Y, s.state.cam.Position().Z)
	s.state.scene.Add(ambientLight)

	// s.state.cam.SetNear(1)
	s.state.cam.SetFar(1000000000000)

	s.state.scene.Add(s.state.cam)
	s.state.scene.Add(helper.NewAxes(0.5))

	window.Get().Subscribe(window.OnKeyDown, func(str string, iface interface{}) {
		keyEvent := iface.(*window.KeyEvent)

		switch keyEvent.Key {
		case window.KeyW:
			s.state.cam.RotateX(-math.Pi / 16)
		case window.KeyS:
			s.state.cam.RotateX(math.Pi / 16)
		case window.KeyA:
			s.state.cam.RotateY(math.Pi / 16)
		case window.KeyD:
			s.state.cam.RotateY(-math.Pi / 16)
		}
	})

	mesh := NewMapPlane(data)

	mesh.SetPosition(0, 0, 0)

	s.state.scene.Add(mesh)

	s.state.app.Gls().ClearColor(0.3, 0.3, 0.7, 1.0)

	s.state.app.Run(func(renderer *renderer.Renderer, deltaTime time.Duration) {
		s.state.app.Gls().Clear(gls.DEPTH_BUFFER_BIT | gls.STENCIL_BUFFER_BIT | gls.COLOR_BUFFER_BIT)
		renderer.Render(s.state.scene, s.state.cam)
	})

	return nil
}
