package render

import (
	"log"

	"bitbucket.org/ataboo/pipetracker/planemock/mapdata"
	"github.com/g3n/engine/geometry"
	"github.com/g3n/engine/gls"
	"github.com/g3n/engine/graphic"
	"github.com/g3n/engine/material"
	"github.com/g3n/engine/math32"
	"github.com/g3n/engine/texture"
	"github.com/golang/geo/s2"
)

func NewMapPlane(data *mapdata.MapData) *graphic.Mesh {
	mapGeo := newMapGeometry(data)
	mat := material.NewStandard(&math32.Color{R: 0.4, G: 0.6, B: 0.4})
	// mat.SetTransparent(true)
	// mat.SetOpacity(0.2)

	text, err := texture.NewTexture2DFromImage("/home/ataboo/Pictures/visual.bmp")
	if err != nil {
		log.Panic(err)
	}
	mat.AddTexture(text)

	// texture.NewTexture2DFromRGBA(mapdata.ValTableToImg(data.Img))
	// mat.AddTexture(tex.NewTexture())
	mesh := graphic.NewMesh(mapGeo, mat)

	return mesh
}

func newMapGeometry(data *mapdata.MapData) *geometry.Geometry {
	mapGeo := geometry.NewGeometry()

	latDeg := 53.0
	longDeg := -114.0

	groundPos := make([][]*math32.Vector3, int(data.Size.Y))

	// Create buffers
	positions := math32.NewArrayF32(0, 16)
	normals := math32.NewArrayF32(0, 16)
	uvs := math32.NewArrayF32(0, 16)
	indices := math32.NewArrayU32(0, 16)

	sizeX := int(data.Size.X)
	sizeY := int(data.Size.Y)

	for iy := 0; iy < sizeY; iy++ {
		groundPos[iy] = make([]*math32.Vector3, int(data.Size.X))
		for ix := 0; ix < sizeX; ix++ {
			lat := latDeg + float64(iy)/3600.0
			long := longDeg + float64(ix)/3600.0
			angle := s2.LatLngFromDegrees(lat, long)
			pos := mapdata.PositionFromLatLng(angle, float64(data.Heights[iy][ix]))
			groundPos[iy][ix] = math32.NewVector3(float32(pos.X), float32(pos.Y), float32(pos.Z))
		}
	}

	for iy := 0; iy < sizeY; iy++ {
		for ix := 0; ix < sizeX; ix++ {
			pos := groundPos[iy][ix]
			positions.AppendVector3(pos)
			norm := math32.NewVec3()
			if iy > 0 {
				norm = norm.AddVectors(norm, pos.Cross(groundPos[iy-1][ix]))
			}

			if iy < sizeY-1 {
				norm = norm.AddVectors(norm, pos.Cross(groundPos[iy+1][ix]))
			}

			if ix > 0 {
				norm = norm.AddVectors(norm, pos.Cross(groundPos[iy][ix-1]))
			}

			if ix < sizeX-1 {
				norm = norm.AddVectors(norm, pos.Cross(groundPos[iy][ix+1]))
			}

			uvs.Append(float32(ix)/data.Size.X, float32(iy)/data.Size.Y)
			// uvs.Append(1, 1)

			if ix < sizeX-1 && iy < sizeY-1 {
				a := ix + sizeX*iy
				b := ix + sizeX*(iy+1)
				c := (ix + 1) + sizeX*(iy+1)
				d := (ix + 1) + sizeX*iy
				// indices.Append(uint32(a), uint32(b), uint32(d))
				// indices.Append(uint32(b), uint32(c), uint32(d))

				indices.Append(uint32(a), uint32(d), uint32(b))
				indices.Append(uint32(b), uint32(d), uint32(c))
			}
		}
	}

	mapGeo.SetIndices(indices)
	mapGeo.AddVBO(gls.NewVBO(positions).AddAttrib(gls.VertexPosition))
	mapGeo.AddVBO(gls.NewVBO(normals).AddAttrib(gls.VertexNormal))
	mapGeo.AddVBO(gls.NewVBO(uvs).AddAttrib(gls.VertexTexcoord))

	return mapGeo
}
