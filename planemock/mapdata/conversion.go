package mapdata

import (
	"github.com/golang/geo/r3"
	"github.com/golang/geo/s2"
	"math"
)

const (
	// EarthR1 radius at the equator in meters
	EarthR1 = 6378137.0
	// EarthR2 radius at the pole in meters
	EarthR2 = 6356752.0

	// EarthR1 = 6378.137
	// EarthR2 = 6356.752
)

// RadiusAtLat get the earth's radius at a specific latitude.
func RadiusAtLat(latRads float64) (meters float64) {
	// R = √ [ (r1² * cos(B))² + (r2² * sin(B))² ] / [ (r1 * cos(B))² + (r2 * sin(B))² ]

	r1CosB := EarthR1 * math.Cos(latRads)
	r2SinB := EarthR2 * math.Sin(latRads)

	return math.Sqrt(((r1CosB*EarthR1)*(r1CosB*EarthR1) + (r2SinB * EarthR2 * r2SinB * EarthR2)) / (r1CosB*r1CosB + r2SinB*r2SinB))
}

// PositionFromLatLng get the cartesian position from the latitude, longitude, and altitude (ASL meters).
func PositionFromLatLng(latLng s2.LatLng, altitudeM float64) r3.Vector {
	latRads := latLng.Lat.Radians()
	longRads := latLng.Lng.Radians()

	radius := RadiusAtLat(latLng.Lat.Radians()) + altitudeM

	return r3.Vector{
		X: radius * math.Cos(latRads) * math.Cos(longRads),
		Y: radius * math.Cos(latRads) * math.Sin(longRads),
		Z: radius * math.Sin(latRads),
	}
}
