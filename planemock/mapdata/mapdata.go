package mapdata

import (
	"encoding/binary"
	"image"
	"image/color"
	"log"
	"math"
	"os"

	"bitbucket.org/ataboo/pipetracker/planemock/helpers"
	"github.com/g3n/engine/math32"
)

const SRTMV3Dim = 3601

type MapData struct {
	Heights [][]int
	Img     [][]int
	Size    math32.Vector2
}

func ReadSrtmV3(heightPath string, imgPath string) (*MapData, error) {
	heights, err := ReadSrtmV3Heights(heightPath)
	if err != nil {
		return nil, err
	}

	img, err := ReadSrtmV3Img(imgPath)
	if err != nil {
		return nil, err
	}

	return &MapData{
		Heights: heights,
		Img:     img,
		Size:    math32.Vector2{X: SRTMV3Dim, Y: SRTMV3Dim},
	}, nil
}

// ReadSrtmV3 parse an SRTM-V3 file to a table of height values.
// The data should contain a height value for every arcsecond between a degree or latitude and longitude (3601 x 3601).
func ReadSrtmV3Heights(path string) ([][]int, error) {
	return ReadBinaryIntTable(path, SRTMV3Dim, SRTMV3Dim, 16)
}

func ReadSrtmV3Img(path string) ([][]int, error) {
	return ReadBinaryIntTable(path, SRTMV3Dim, SRTMV3Dim, 8)
}

func ReadBinaryIntTable(filePath string, width int, height int, intBitCount int) ([][]int, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var buffer interface{}

	switch intBitCount {
	case 8:
		buffer = make([]uint8, width)
	case 16:
		buffer = make([]uint16, width)
	default:
		log.Panic("unsupported intBitCount")
	}

	vals := make([][]int, height)
	for y := 0; y < height; y++ {
		if err := binary.Read(file, binary.BigEndian, buffer); err != nil {
			return vals, err
		}

		vals[y] = make([]int, width)
		for x := 0; x < width; x++ {
			if intBitCount == 8 {
				vals[y][x] = int((buffer.([]uint8))[x])
			} else if intBitCount == 16 {
				vals[y][x] = int((buffer.([]uint16))[x])
			}
		}
	}

	return vals, nil
}

// ValTableToImg render a table of heights to a grayscale image normalized to the min and max values.
func ValTableToImg(heightVals [][]int) image.Image {
	width := len(heightVals)

	img := image.NewGray16(image.Rect(0, 0, width, width))

	min := math.MaxInt64
	max := math.MinInt64

	// Get the min and max values.
	for _, row := range heightVals {
		if len(row) != width {
			log.Panic("height values should be square.")
		}

		for _, val := range row {
			min = helpers.MinInt(val, min)
			max = helpers.MaxInt(val, max)
		}
	}

	heightRange := max - min
	for y := 0; y < width; y++ {
		for x := 0; x < width; x++ {
			height := heightVals[y][x]
			heightNormalized := float64(height-min) / float64(heightRange) * float64(math.MaxUint16)

			img.SetGray16(x, y, color.Gray16{
				Y: uint16(heightNormalized),
			})
		}
	}

	return img
}
