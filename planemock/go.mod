module bitbucket.org/ataboo/pipetracker/planemock

go 1.13

require (
	github.com/g3n/engine v0.1.1-0.20191001141548-13e8582dadaa
	github.com/golang/geo v0.0.0-20190916061304-5b978397cfec
	github.com/westphae/quaternion v0.0.0-20190804192539-eb19a71cb818
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
