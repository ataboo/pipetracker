package utils

import (
	"io"
	"sync"
)

type LineReader struct {
	input      io.Reader
	outChan    chan []byte
	Delimiter  byte
	lock       sync.Mutex
	pumpLock   sync.Mutex
	BufferSize int

	carry         []byte
	buffer        []byte
	readFromInput bool
}

func NewLineReader(input io.Reader) *LineReader {
	return &LineReader{
		input:      input,
		BufferSize: 128,
	}
}

func (r *LineReader) ReadUntil(delimiter byte) []byte {
	r.lock.Lock()
	defer r.lock.Unlock()

	r.Delimiter = delimiter

	if r.carry == nil {
		r.carry = make([]byte, 0)
		r.readFromInput = true
	}

	r.buffer = make([]byte, r.BufferSize)

	return r.linePump()
}

func (r *LineReader) ReadLine() []byte {
	return r.ReadUntil('\n')
}

func (r *LineReader) ReadUntilString(delimiter byte) string {
	return string(r.ReadUntil(delimiter))
}

func (r *LineReader) linePump() []byte {
	for {
		if r.readFromInput {
			n, err := r.input.Read(r.buffer)
			if err != nil {
				return nil
			}

			r.carry = append(r.carry, r.buffer[:n]...)
			r.readFromInput = false
		}

		if idx := r.indexOfChar(r.carry, r.Delimiter); idx > 0 {
			line := r.carry[:idx]
			r.carry = r.carry[idx+1:]
			return line
		} else {
			r.readFromInput = true
		}
	}
}

func (r *LineReader) indexOfChar(subject []byte, char byte) int {
	for i := 0; i < len(subject); i++ {
		if subject[i] == char {
			return i
		}
	}

	return -1
}
