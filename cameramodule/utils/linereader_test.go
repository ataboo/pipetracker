package utils

import (
	"bytes"
	"strings"
	"testing"
)

func TestLineReader(t *testing.T) {
	source := bytes.NewBufferString("line1\nline2\nblah")

	lineReader := NewLineReader(source)

	line := lineReader.ReadLine()
	if string(line) != "line1" {
		t.Error("unexpected line", string(line))
	}

	line2 := lineReader.ReadLine()
	if string(line2) != "line2" {
		t.Error("unexpected line", string(line2))
	}

	line3 := lineReader.ReadLine()
	if line3 != nil {
		t.Error("expected error")
	}
}

func TestLineReaderMultipleReads(t *testing.T) {
	firstLine := "This is the first line.  It is 50 characters long."
	secondLine := "This is the second line.  It is 51 characters long."
	thirdLine := "This is the third line.  Unlike the first two lines, it is 78 characters long."
	source := bytes.NewBufferString(strings.Join([]string{firstLine, secondLine, thirdLine}, "\n"))

	reader := NewLineReader(source)
	reader.BufferSize = 16

	firstRead := reader.ReadLine()
	if string(firstRead) != firstLine {
		t.Error("unexpected first read", firstRead)
	}

	secondRead := reader.ReadUntilString('\n')
	if secondRead != secondLine {
		t.Error("unexpected second line", secondRead)
	}

	thirdRead := reader.ReadUntilString('\n')
	if thirdRead != "" {
		t.Error("expected nil third read")
	}
}
