package utils

import (
	"math"
	"sync"
	"testing"
	"time"

	"github.com/westphae/quaternion"
)

func TestMxChan(t *testing.T) {
	mxChan := NewMxChan()
	firstCount := 0
	secondCount := 0
	wait := sync.WaitGroup{}

	err := mxChan.Start()
	if err == nil {
		t.Error("expected error")
	}

	mxChan.Send(0)

	if firstCount != 0 && secondCount != 0 {
		t.Error("unexpected count")
	}

	firstChan := mxChan.Subscribe()

	go func() {
		for _ = range firstChan {
			firstCount++
			wait.Done()
		}
	}()

	wait.Add(1)
	mxChan.Send(0)
	wait.Wait()

	if firstCount != 1 {
		t.Error("unexpected first count", firstCount)
	}

	secondChan := mxChan.Subscribe()

	go func() {
		select {
		case <-secondChan:
			secondCount++
		}
	}()

	wait.Add(1)
	mxChan.Send(0)
	wait.Wait()

	if firstCount != 2 {
		t.Error("unexpected first count", firstCount)
	}

	if secondCount != 1 {
		t.Error("unexpected secondCount", secondCount)
	}

	if len(mxChan.outputs) != 2 {
		t.Error("unexpected output count", len(mxChan.outputs))
	}

	wait.Add(1)
	mxChan.Send(0)
	wait.Wait()

	if firstCount != 3 {
		t.Error("unexpected first count", firstCount)
	}

	if secondCount != 1 {
		t.Error("unexpected second count")
	}

	if len(mxChan.outputs) != 1 {
		t.Error("unexpected output count", len(mxChan.outputs))
	}
}

func TestMxChanQuaternion(t *testing.T) {
	firstQ := quaternion.FromEuler(90, 90, 90)
	var outVal *quaternion.Quaternion

	mxChan := NewMxChan()
	var outChan = mxChan.Subscribe()

	go func() {
		select {
		case val := <-outChan:
			quatVal, ok := val.(*quaternion.Quaternion)
			if !ok {
				t.Error("failed to cast to quaternion")
			}
			if quatVal == nil {
				t.Error("nil quat val")
			}
			outVal = quatVal
		}
	}()

	mxChan.Send(&firstQ)

	time.Sleep(time.Millisecond)

	if outVal != &firstQ {
		t.Error("quaternions don't match", outVal, &firstQ)
	}
}

func TestQuatProduct(t *testing.T) {
	firstAngle := quaternion.FromEuler(math.Pi/2, 0, 0)
	secondAngle := quaternion.FromEuler(math.Pi/4, 0, 0)

	product := quaternion.Prod(firstAngle, secondAngle)
	threeQuarterPi := quaternion.FromEuler(math.Pi*3/4, 0, 0)

	if product != threeQuarterPi {
		t.Error("unexpected product", product, threeQuarterPi)
	}
}

func TestQuatDiff(t *testing.T) {
	firstAngle := quaternion.FromEuler(math.Pi/6, 0, 0)
	secondAngle := quaternion.FromEuler(math.Pi, 0, 0)

	diffAngle := quaternion.Prod(firstAngle.Conj(), secondAngle)
	expectedDiff := quaternion.FromEuler(math.Pi*5/6, 0, 0)
	if !CloseEnoughQuat(diffAngle, expectedDiff, 1e-8) {
		t.Error("unexpected diff", diffAngle, expectedDiff)
	}
}
