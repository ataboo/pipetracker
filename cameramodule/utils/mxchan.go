package utils

import (
	"fmt"
	"sync"
	"time"
)

func NewMxChan() *MxChan {
	mxChan := &MxChan{
		started:   false,
		outputs:   make([]chan interface{}, 0),
		inputChan: make(chan interface{}),
	}

	mxChan.Start()

	return mxChan
}

type MxChan struct {
	started   bool
	outputs   []chan interface{}
	inputChan chan interface{}
	lock      sync.Mutex
	waitGroup sync.WaitGroup
}

func (m *MxChan) Subscribe() <-chan interface{} {
	outChan := make(chan interface{})
	m.outputs = append(m.outputs, outChan)

	return outChan
}

func (m *MxChan) Send(val interface{}) {
	m.waitGroup.Add(1)
	m.inputChan <- val
	m.waitGroup.Wait()
}

func (m *MxChan) Start() error {
	if m.started {
		return fmt.Errorf("Already started")
	}
	m.started = true

	go func() {
		for val := range m.inputChan {
			m.lock.Lock()
			aliveChannels := make([]chan interface{}, 0)
			for _, outChan := range m.outputs {
				select {
				case outChan <- val:
					aliveChannels = append(aliveChannels, outChan)
				case <-time.After(time.Millisecond):
					close(outChan)
				}
			}
			m.outputs = aliveChannels
			m.lock.Unlock()
			m.waitGroup.Done()
		}

	}()

	return nil
}
