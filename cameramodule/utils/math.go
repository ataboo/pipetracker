package utils

import (
	"math"
	"math/rand"

	"github.com/westphae/quaternion"
)

func CloseEnoughQuat(a quaternion.Quaternion, b quaternion.Quaternion, threshold float64) bool {
	return CloseEnough64(a.W, b.W, threshold) && CloseEnough64(a.X, b.X, threshold) && CloseEnough64(a.Y, b.Y, threshold) && CloseEnough64(a.Z, b.Z, threshold)
}

func CloseEnough64(a float64, b float64, threshold float64) bool {
	return math.Abs(a-b) <= threshold
}

func DeviatedAngle(angle quaternion.Quaternion, deviation float64, rng *rand.Rand) quaternion.Quaternion {
	return quaternion.New(
		RandInRange(-deviation, deviation, rng)+angle.W,
		RandInRange(-deviation, deviation, rng)+angle.X,
		RandInRange(-deviation, deviation, rng)+angle.Y,
		RandInRange(-deviation, deviation, rng)+angle.Z,
	)
}

func RandInRange(a float64, b float64, rng *rand.Rand) float64 {
	max := math.Max(a, b)
	min := math.Min(a, b)
	span := max - min

	return rng.Float64()*span + min
}
