package video

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"

	"bitbucket.org/ataboo/pipetracker/planemock/planedata"
)

func StartTcpHost() {
	address := "localhost:3001"

	listener, err := net.Listen("tcp", address)
	if err != nil {
		log.Panic(err)
	}

	conn, err := listener.Accept()
	if err != nil {
		log.Panic(err)
	}

	rawBytes, err := ioutil.ReadAll(conn)
	data := planedata.PlaneData{}
	err = json.Unmarshal(rawBytes, &data)

	decoded, err := data.DecodeImg()

	fmt.Printf("Read: %s\n", string(decoded))
}
