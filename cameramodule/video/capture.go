package video

import (
	"bytes"
	"fmt"
	"image"
	"log"
	"os"
	"strconv"

	"image/jpeg"
	"image/png"

	"github.com/blackjack/webcam"
)

const YUYV422 = webcam.PixelFormat(0x56595559)
const MotionJpeg = webcam.PixelFormat(0x47504A4D)

func CaptureFrames(imgCount int, outPath string, format webcam.PixelFormat) {
	os.MkdirAll(outPath, 0700)

	cam, err := webcam.Open("/dev/video0")
	if err != nil {
		panic(err)
	}
	defer cam.Close()

	w, h := setImageFormat(cam, format)

	err = cam.StartStreaming()
	if err != nil {
		panic(err.Error())
	}

	for i := 0; i < imgCount; i++ {
		err = cam.WaitForFrame(1)

		if format == MotionJpeg {
			rawJpg, err := cam.ReadFrame()
			if err != nil {
				log.Fatal(err)
			}

			writeJpg(rawJpg, outPath, i)
		} else {
			img := captureFrame(cam, format, int(w), int(h))
			writeImg(img, outPath, i)
		}

		switch err.(type) {
		case nil:
		case *webcam.Timeout:
			fmt.Fprint(os.Stderr, err.Error())
			continue
		default:
			panic(err.Error())
		}

	}
}

func captureFrame(cam *webcam.Webcam, format webcam.PixelFormat, w int, h int) image.Image {
	frame, err := cam.ReadFrame()
	if len(frame) != 0 {
		switch format {
		case YUYV422:
			return parseYUYV422(frame, w, h)
		case MotionJpeg:
			return parseMJpeg(frame, w, h)
		default:
			log.Fatal("format not supported")
		}
	} else if err != nil {
		panic(err.Error())
	}

	panic("failed capture")
}

func setImageFormat(cam *webcam.Webcam, format webcam.PixelFormat) (w uint32, h uint32) {
	// fmt.Print("Formats: ")
	// for k, format := range cam.GetSupportedFormats() {
	// 	fmt.Printf("\n\t%X: %s, ", k, format)
	// }
	// fmt.Print("\nSizes: ")
	// for k, size := range cam.GetSupportedFrameSizes(format) {
	// 	fmt.Printf("\n\t%d: %+v, ", k, size)
	// }

	_, w, h, err := cam.SetImageFormat(format, 1280, 720)
	if err != nil {
		log.Fatal(err)
	}

	// fmt.Printf("\nFormat: %+v, w: %d, h: %d", f, w, h)

	return w, h
}

func parseYUYV422(frame []byte, w int, h int) *image.YCbCr {
	fmt.Printf("frame len: %d\n", len(frame))

	img := image.NewYCbCr(image.Rect(0, 0, w, h), image.YCbCrSubsampleRatio422)
	for i := range img.Cb {
		fIdx := i * 4
		img.Y[i*2] = frame[fIdx]
		img.Y[i*2+1] = frame[fIdx+2]
		img.Cb[i] = frame[fIdx+1]
		img.Cr[i] = frame[fIdx+3]
	}

	return img
}

func parseMJpeg(jBytes []byte, w int, h int) image.Image {
	fmt.Printf("w: %d, h: %d, count: %d\n", w, h, len(jBytes))

	reader := bytes.NewBuffer(jBytes)

	img, err := jpeg.Decode(reader)
	if err != nil {
		log.Fatal(err)
	}

	return img
}

func writeImg(img image.Image, baseDir string, idx int) {
	imgPath := baseDir + "/pipetrack_" + strconv.Itoa(idx) + ".png"

	f, err := os.Create(imgPath)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	if err := png.Encode(f, img); err != nil {
		panic(err)
	}
}

func writeJpg(rawJpg []byte, baseDir string, idx int) {
	imgPath := baseDir + "/pipetrack_" + strconv.Itoa(idx) + ".jpg"

	f, err := os.Create(imgPath)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	_, err = f.Write(rawJpg)
	if err != nil {
		log.Fatal(err)
	}
}
