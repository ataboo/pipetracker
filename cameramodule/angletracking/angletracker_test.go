package angletracking

import (
	"encoding/json"
	"fmt"
	"math"
	"testing"
	"time"

	"bitbucket.org/ataboo/pipetracker/cameramodule/utils"
	"github.com/westphae/quaternion"
)

func TestFakeAngleTracker(t *testing.T) {
	fakeAngle, err := NewFakeAngleTracker(&FakePortConfig{
		Angle:     quaternion.FromEuler(math.Pi/2.0, 0, 0),
		Deviation: 0.0,
	})
	if err != nil {
		t.Error(err)
	}

	angleChan, err := fakeAngle.Start()
	if err != nil {
		t.Error(err)
	}

	select {
	case angle := <-angleChan:
		if angle.Angle != quaternion.FromEuler(math.Pi/2.0, 0, 0) {
			t.Error("unexpected angle", angle.Angle)
		}

	case <-time.After(time.Second):
		t.Error("timed out")
	}

}

func TestBlockedReader(t *testing.T) {
	reader := &blockedReader{make(chan []byte)}
	lineReader := utils.NewLineReader(reader)

	go func() {
		time.Sleep(time.Millisecond * 1)
		reader.Input <- []byte("hello")
		time.Sleep(time.Millisecond * 1)
		reader.Input <- []byte(" bytes?")
	}()

	line := lineReader.ReadUntil('?')

	if string(line) != "hello bytes" {
		t.Error("unexpected line", string(line))
	}
}

func TestReadingQuaternion(t *testing.T) {
	reader := &blockedReader{make(chan []byte)}
	lineReader := utils.NewLineReader(reader)
	quat1 := quaternion.FromEuler(90, 0, 0)
	quat2 := quaternion.FromEuler(0, 90, 0)
	quat1Encoded, _ := json.Marshal(quat1)
	quat2Encoded, _ := json.Marshal(quat2)

	readOutput := append(quat1Encoded, []byte("|")...)
	readOutput = append(readOutput, quat2Encoded...)
	readOutput = append(readOutput, []byte("|")...)

	splitIdx := len(readOutput) / 4

	go func() {
		time.Sleep(time.Millisecond * 1)
		reader.Input <- readOutput[:splitIdx]
		time.Sleep(time.Millisecond * 1)
		reader.Input <- readOutput[splitIdx:]
	}()

	read1 := lineReader.ReadUntil('|')
	read2 := lineReader.ReadUntil('|')

	var decoded1 quaternion.Quaternion
	var decoded2 quaternion.Quaternion

	if err := json.Unmarshal(read1, &decoded1); err != nil {
		t.Error("unexpected error", err)
	}

	if err := json.Unmarshal(read2, &decoded2); err != nil {
		t.Error("unexpected error", err)
	}

	if decoded1 != quat1 {
		t.Error("expected match", decoded1, quat1)
	}

	if decoded2 != quat2 {
		t.Error("expected match", decoded2, quat2)
	}
}

type blockedReader struct {
	Input chan []byte
}

func (b *blockedReader) Read(p []byte) (n int, err error) {
	val := <-b.Input
	if len(p) < len(val) {
		panic("buffer must not be smaller than channel values")
	}

	return copy(p, val), nil
}

func (b *blockedReader) Close() error {
	close(b.Input)

	return nil
}

func (b *blockedReader) Flush() error {
	select {
	case <-b.Input:
		return nil
	case <-time.After(time.Millisecond):
		return fmt.Errorf("timed out")
	}
}
