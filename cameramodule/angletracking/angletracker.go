package angletracking

import (
	"encoding/json"
	"fmt"
	"io"
	"sync"

	"bitbucket.org/ataboo/pipetracker/cameramodule/utils"
	"github.com/tarm/serial"
	"github.com/westphae/quaternion"
)

type PortReader interface {
	io.ReadCloser
	Flush() error
}

type AngleData struct {
	Angle quaternion.Quaternion `json:"angle"`
}

type AngleTracker struct {
	port       PortReader
	lineReader *utils.LineReader
	mxChannel  *utils.MxChan
	once       *sync.Once
	outChan    chan *AngleData
	stopChan   chan int
}

func NewSerialAngleTracker(cfg *serial.Config) (*AngleTracker, error) {
	serialPort, err := serial.OpenPort(cfg)
	if err != nil {
		return nil, err
	}

	return NewAngleTracker(serialPort), nil
}

func NewFakeAngleTracker(cfg *FakePortConfig) (*AngleTracker, error) {
	var fakePort = NewFakePortReader(cfg)
	if err := fakePort.Open(); err != nil {
		return nil, err
	}

	return NewAngleTracker(fakePort), nil
}

func NewAngleTracker(portReader PortReader) *AngleTracker {
	return &AngleTracker{
		port:       portReader,
		lineReader: utils.NewLineReader(portReader),
	}
}

func (e *AngleTracker) Start() (<-chan *AngleData, error) {
	if e.outChan != nil {
		return nil, fmt.Errorf("already started")
	}

	e.outChan = make(chan *AngleData)
	e.port.Flush()
	e.startReadLoop()

	return e.outChan, nil
}

func (e *AngleTracker) Close() error {
	if e.outChan == nil {
		return fmt.Errorf("not started")
	}

	err := e.port.Close()
	e.stopChan <- 0
	close(e.outChan)
	close(e.stopChan)

	return err
}

func (e *AngleTracker) startReadLoop() error {
	if e.stopChan != nil {
		return fmt.Errorf("already started read loop")
	}

	e.stopChan = make(chan int)
	go func() {
		for {
			rawLine := e.lineReader.ReadUntil('|')

			fmt.Println(string(rawLine))

			quatAngle := quaternion.Quaternion{}
			if err := json.Unmarshal(rawLine, &quatAngle); err != nil {
				fmt.Println("Malformed serial row: ", string(rawLine), err)
				continue
			}

			select {
			case e.outChan <- &AngleData{Angle: quatAngle}:
				//
			case <-e.stopChan:
				return
			}
		}
	}()

	return nil
}
