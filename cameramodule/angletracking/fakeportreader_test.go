package angletracking

import (
	"encoding/json"
	"math"
	"math/rand"
	"testing"
	"time"

	"bitbucket.org/ataboo/pipetracker/cameramodule/utils"
	"github.com/westphae/quaternion"
)

func TestFakePortReader(t *testing.T) {
	cfg := &FakePortConfig{
		Angle: quaternion.FromEuler(math.Pi/2.0, 0, 0),
	}
	reader := NewFakePortReader(cfg)
	reader.rng = rand.New(rand.NewSource(42))
	reader.tickDelay = time.Millisecond * 2

	lineReader := utils.NewLineReader(reader)

	if err := reader.Close(); err == nil {
		t.Error("expected error")
	}

	if len(reader.buffer) != 0 {
		t.Error("expected empty buffer")
	}

	if err := reader.Open(); err != nil {
		t.Error(err)
	}

	if err := reader.Open(); err == nil {
		t.Error("expected error")
	}

	rawLine := lineReader.ReadUntil('|')

	if len(rawLine) == 0 {
		t.Error("expected values")
	}

	angle := quaternion.Quaternion{}
	if err := json.Unmarshal(rawLine, &angle); err != nil {
		t.Error(err)
	}

	if angle.X-cfg.Angle.X > 0.01 {
		t.Error("Angle out of range")
	}
}
