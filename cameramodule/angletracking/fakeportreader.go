package angletracking

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"sync"
	"time"

	"bitbucket.org/ataboo/pipetracker/cameramodule/utils"
	"github.com/westphae/quaternion"
)

func NewFakePortReader(cfg *FakePortConfig) *FakePortReader {
	return &FakePortReader{
		cfg:       cfg,
		rng:       rand.New(rand.NewSource(time.Now().Unix())),
		tickDelay: time.Millisecond * 20,
		buffer:    make([]byte, 0),
	}
}

type FakePortConfig struct {
	Angle     quaternion.Quaternion
	Deviation float64
}

type FakePortReader struct {
	cfg       *FakePortConfig
	rng       *rand.Rand
	stopChan  chan int
	tickDelay time.Duration
	buffer    []byte
	lock      sync.Mutex
}

func (f *FakePortReader) Open() error {
	if f.stopChan != nil {
		return fmt.Errorf("already started")
	}

	f.startAngleLoop()

	return nil
}

func (f *FakePortReader) Read(p []byte) (n int, err error) {
	if f.stopChan == nil {
		return 0, fmt.Errorf("not started")
	}

	defer f.lock.Unlock()
	f.lock.Lock()

	n = copy(p, f.buffer)
	f.buffer = f.buffer[n:]

	return n, nil
}

func (f *FakePortReader) Close() error {
	if f.stopChan == nil {
		return fmt.Errorf("not started")
	}
	f.stopChan <- 0

	return nil
}

func (f *FakePortReader) Flush() error {
	if f.stopChan == nil {
		return fmt.Errorf("not started")
	}
	f.buffer = make([]byte, 0)

	return nil
}

func (f *FakePortReader) startAngleLoop() {
	f.stopChan = make(chan int)

	go func() {
		ticker := time.NewTicker(f.tickDelay)
		for {
			f.lock.Lock()
			select {
			case <-f.stopChan:
				close(f.stopChan)
				return
			case <-ticker.C:
				f.buffer = append(f.buffer, f.getAngleBytes()...)
			}
			f.lock.Unlock()
		}
	}()
}

func (f *FakePortReader) getAngleBytes() []byte {
	angle := utils.DeviatedAngle(f.cfg.Angle, f.cfg.Deviation, f.rng)
	encoded, _ := json.Marshal(angle)

	return append(encoded, []byte("|")...)
}
