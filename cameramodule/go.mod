module bitbucket.org/ataboo/pipetracker/cameramodule

go 1.13

require (
	bitbucket.org/ataboo/pipetracker/planemock v0.0.0
	github.com/blackjack/webcam v0.0.0-20190407142958-6cd3de4f4861
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	github.com/westphae/quaternion v0.0.0-20190804192539-eb19a71cb818
	golang.org/x/sys v0.0.0-20191029155521-f43be2a4598c // indirect
)

replace bitbucket.org/ataboo/pipetracker/planemock v0.0.0 => ./../planemock
